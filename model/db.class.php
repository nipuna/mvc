<?php

class db
{

    /*** Declare instance ***/
    private static $instance = NULL;
    static private $_DEBUGMODE = false;
    private $connection;
    private static $_instance; //The single instance
    private $host = "localhost";
    private $username = "root";
    private $password = "";
    private $database = "amynmoney";

    /**
     *
     * the constructor is set to private so
     * so nobody can create a new instance using new
     *
     */
    private function __construct()
    {

        $this->connection = new mysqli($this->host, $this->username, $this->password, $this->database);

        if (mysqli_connect_error()) {
            printf("Connect failed: %s\n", $this->connection->connect_error);
            exit();
        }
    }

    /**
     *
     * Return DB instance or create intitial connection
     *
     * @return object (PDO)
     *
     * @access public
     *
     */
    public static function getInstance()
    {
        if (!self::$instance) { // If no instance then make one
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getConnection()
    {
        return $this->connection;
    }

}

