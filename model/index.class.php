<?php

class index extends baseModel
{

    private $vars = array();

    static private $_tablename = 'tbl_bank';
    static private $_primarykey = 'bank_id';


    public function __set($index, $value)
    {
        $this->vars[$index] = $value;
    }

    public function __get($index)
    {
        return $this->vars[$index];
    }

    public function insert()
    {
        $insArray["cat_name"] = $_POST['name'];
        $insArray["cat_name"] = $this->name;
        $insArray["address"] = $this->address;
        $user_id = db::queryInsert(self::$_tablename, $insArray);

        return $user_id;
    }

    public function update()
    {
        $insArray["id"] = $this->id;
        $insArray["title"] = $this->title;
        $insArray["name"] = $this->name;
        $insArray["address"] = $this->address;

        $id = DBAccess::queryUpdate(self::$_tablename, $insArray, self::$_primarykey . '=' . $this->user_id);

        return $id;
    }

    public function delete($id)
    {
        $id = db::queryDelete(self::$_tablename, self::$_primarykey . '=' . $id);

        return $id;
    }

    public function find($user_id)
    {
        $result = $this->db->querySelect(self::$_tablename, self::$_primarykey . '=' . $user_id);
        $row = $result->fetch_row();

        return $row;
    }

    public function findAll($wh = null, $arrFields = "*", $sort = array(), $sortBy = "A", $lStart = 0, $numRecs = 0, $status = array(1, 0))
    {
        $result = $this->db->querySelect(self::$_tablename, $wh, $arrFields, $sort, $sortBy, $lStart, $numRecs, $status);

        return $result;
    }

    public function InnerJoinAll($tables, $wh, $arrFields = "*", $sort = array(), $sortBy = "A", $lStart = 0, $numRecs = 0, $status = array(1, 0))
    {
        $tables = self::$_tablename . "," . implode(",", $tables);
        $wh = implode(" and ", $wh);
        $result = db::querySelect($tables, $wh, $arrFields, $sort, $sortBy, $lStart, $numRecs, $status);

        return $result;
    }
}