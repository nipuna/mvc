<?php

class indexController extends baseController {

public function index($model) {
    
  
		$this->registry->template->heading = 'Index Page';
		$this->registry->template->thisPages = 'index';
		$this->registry->template->model= $model;
		$this->registry->template->content = 'index/view.phtml';
		
		$this->authRequire = false;
		$result = $model->findAll();
		// while($row = $result->fetch_assoc()) {
		// 	print ($row['bank']);
		// }
				
		$this->registry->template->show('layout1');        
}

}	