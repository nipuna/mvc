<?php

class commonDb
{

    private $connection;

    /**
     * commonDb constructor.
     * @param $connection
     */
    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param $tblName
     * @param $insArr
     * @return int
     */
    public function queryInsert($tblName, $insArr)
    {
        $fNString = implode(",", array_keys($insArr));
        $insArr = array_map('addslashes', $insArr);

        $fValString = "'" . implode("','", $insArr) . "'";
        $sqlString = "insert into $tblName ($fNString) values ($fValString)";
        if (self::$_DEBUGMODE) {
            die($sqlString);
            echo $sqlString;
        } else {


            $result = mysql_query($sqlString) or die($sqlString . mysql_error());
            return mysql_insert_id();

        }
    }

    /**
     * @param $tblName
     * @param $insArr
     * @param string $condStr
     * @return resource
     */
    public function queryUpdate($tblName, $insArr, $condStr = "")
    {
        $fUpString = array();
        foreach ($insArr as $key => $val) {
            $fUpString[] = $key . "='" . $val . "'";
        }
        $fUpString = implode(",", $fUpString);
        if ($condStr) {
            $sqlString = "update $tblName set $fUpString where $condStr";
        } else {
            $sqlString = "update $tblName set $fUpString";
        }

        if (self::$_DEBUGMODE) {
            die($sqlString);
        } else {

            $result = mysql_query($sqlString) or die(mysql_error());
            return $result;

        }
    }

    /**
     * @param $tableName
     * @param bool $arrCon
     * @param string $arrFields
     * @param array $sort
     * @param string $sortBy
     * @param int $lStart
     * @param int $numRecs
     * @param array $status
     * @return mixed
     */
    public function querySelect($tableName, $arrCon = false, $arrFields = "*", $sort = array(), $sortBy = "A", $lStart = 0, $numRecs = 0, $status = array(1, 0))
    {
        $fNames = is_array($arrFields) ? implode(",", $arrFields) : $arrFields;
        if ($arrCon !== false && $arrCon != "") {
            $query = "select $fNames from $tableName where $arrCon";

        } else {
            $query = "select $fNames from $tableName";

        }
        if ($sort) {
            $strSort = implode(",", $sort);
            $query .= " order by $strSort";
            if ($sortBy == "D") {
                $query .= " desc";
            } elseif ($sortBy == "A") {
                $query .= " asc";
            }
        }
        if ($numRecs) {
            $query .= " limit $lStart,$numRecs";
        }

        $result = $this->connection->query($query);
        if (mysql_errno() != 0) {
            print mysql_error();
            die(mysql_errno());
        }

        return $result;
    }

    /**
     * @param $tblName
     * @param string $condStr
     * @return resource
     */
    public function queryDelete($tblName, $condStr = "")
    {


        $fDString = "delete from $tblName ";
        if ($condStr != "") {
            $fDString .= " where $condStr";
        }
        if (self::$_DEBUGMODE) {
            die($fDString);
        }


        $result = mysql_query($fDString) or die(mysql_error());
        return $result;

    }
}
