<?php

class Registry
{

    /*
    * @the vars array
    * @access private
    */
    private $vars = [];


    /**
     * @param $index
     * @param $value
     */
    public function __set($index, $value)
    {
        $this->vars[$index] = $value;
    }

    /**
     * @param $index
     * @return mixed
     */
    public function __get($index)
    {
        return $this->vars[$index];
    }

}

