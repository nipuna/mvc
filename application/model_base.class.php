<?php

abstract class baseModel
{

    protected $registry;
    protected $db;

    function __construct($registry)
    {
        $this->registry = $registry;
        $this->db = new commonDb($registry->db);
    }

    abstract function insert();

    abstract function update();

    abstract function delete($id);

    abstract function find($user_id);

    abstract function findAll();
}

