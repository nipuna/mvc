<?php

class router
{
    /*
    * @the registry
    */


    private $path;

    public $file;

    public $controller;

    public $action;

    function __construct($registry)
    {

        $this->registry = $registry;

    }

    /**
     * @param $path
     * @throws Exception
     */
    function setPath($path)
    {

        /*** check if path i sa directory ***/
        if (is_dir($path) == false) {
            throw new Exception ('Invalid controller path: `' . $path . '`');
        }
        /*** set the path ***/
        $this->path = $path;
    }


    public function loader()
    {
        /*** check the route ***/

        $this->getController();

        /*** if the file is not there diaf ***/

        if (is_readable($this->file) == false) {
            $this->file = $this->path . '/error404.php';
            $this->controller = 'error404';
        }

        /*** include the controller ***/

        include $this->file;

        /*** a new controller class instance ***/
        $class = $this->controller . 'Controller';
        $model = $this->controller;


        $controller = new $class($this->registry);


        /*** check if the action is callable ***/

        if (is_callable(array($controller, $this->action)) == false) {
            $action = 'index';
        } else {
            $action = $this->action;
        }

        /*** run the action ***/

        $model = new $model($this->registry);

        $controller->$action($model);

        if ($controller->getAuthRequire()) {
        	$controller->checkUser();
        }

    }

    /**
     *
     * @get the controller
     *
     * @access private
     *
     * @return void
     *
     */
    private function getController()
    {

        /*** get the route from the url ***/
        $route = (empty($_GET['rt'])) ? '' : $_GET['rt'];
        $x = 0;
        if (empty($route)) {
            $route = 'index';
        } else {
            /*** get the parts of the route ***/

            $parts = explode('/', $route);

            $this->controller = $parts[$x];

            if (isset($parts[($x + 1)])) {
                if ($x == 1) {
                    $this->action = $parts[0] . "_" . $parts[($x + 1)];
                } else {
                    $this->action = $parts[($x + 1)];
                }
            }

            if (isset($parts[($x + 2)])) {
                $this->registry->page = $parts[($x + 2)];

            } else {
                $this->registry->page = "";
            }

            if (isset($parts[($x + 3)])) {
                $this->registry->page2 = $parts[($x + 3)];

            } else {
                $this->registry->page2 = "";
            }
        }

        if (empty($this->controller)) {
            $this->controller = 'index';
        }
        /*** Get action ***/

        if (empty($this->action)) {
            if (isset($parts[$x])) {
                $this->action = 'index';
            }
        }

        /*** set the file path ***/
        $this->file = $this->path . '/' . $this->controller . 'Controller.php';
        $this->registry->controller = $this->controller;
        $this->registry->this_url = WEB_ROOT . $route . ".htm";
    }

}
