<?php

abstract class baseController
{
    use auth;
    /*
     * @registry object
     */
    protected $registry;
    protected $authRequire = true;

    /**
     * baseController constructor.
     * @param $registry
     */
    function __construct($registry)
    {
        $this->registry = $registry;
    }

    public function getAuthRequire()
    {
        return $this->authRequire;
    }

    //public function check
    /**
     * @param $model
     * @return mixed
     */
    abstract function index($model);

}

