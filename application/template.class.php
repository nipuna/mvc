<?php

class Template
{

    /*
     * @the registry
     * @access private
     */
    private $registry;

    /*
     * @Variables array
     * @access private
     */
    private $vars = array();

    /**
     * Template constructor.
     * @param $registry
     */
    function __construct($registry)
    {
        $this->registry = $registry;

    }


    /**
     * @param $index
     * @param $value
     */
    public function __set($index, $value)
    {
        $this->vars[$index] = $value;
    }

    /**
     * @param $name
     * @return bool
     * @throws Exception
     */
    function show($name)
    {

        $path = __SITE_PATH . '/views' . '/' . $name . '.phtml';

        if (file_exists($path) == false) {
            throw new Exception('Template not found in ' . $path);
            return false;
        }

        // Load variables
        foreach ($this->vars as $key => $value) {
            $$key = $value;

        }

        include($path);
    }
}
